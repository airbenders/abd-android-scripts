#!/usr/bin/env bash

while getopts 'a:p:?s:?' opt
do
    case ${opt} in
        a) APK_FILE=${OPTARG} ;;
        p) PREFIX=${OPTARG} ;;
        s) SUFFIX=${OPTARG} ;;
        \?)
            echo "ABD APK renamer"
            echo ""
            echo "Usages:"
            echo "  abd-apk-rename.sh -a APK_FILE -p PREFIX -s SUFFIX"
            echo ""
            echo "Options:"
            echo "  -a APK_FILE             APK file to rename."
            echo "  -p PREFIX               Prefix for the apk name."
            echo "  -s SUFFIX               Suffix for the apk name."
            echo ""
            exit
            ;;
    esac
done

if [[ "${APK_FILE}" = "" ]]
then
    echo "-a APK File is required."
    exit 1
fi

# ==================================================================================================

source $(dirname $0)/abd-base-apk.sh ${APK_FILE}

# ==================================================================================================

echo
echo "OPERATION"
echo

NEW_APK_FILE_NAME="${PREFIX}${VERSION_CODE}${SUFFIX}.apk"
echo "    * New APK file name: ${NEW_APK_FILE_NAME}"

mv ${APK_FILE} "${APK_FILE%/*}/${NEW_APK_FILE_NAME}"

echo
echo "$NEW_APK_FILE_NAME"

