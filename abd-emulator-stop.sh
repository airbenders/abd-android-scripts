#!/usr/bin/env bash

PORT=""

while getopts 'p:?' opt
do
    case ${opt} in
        p) PORT=${OPTARG} ;;
        \?)
            echo "ABD Emulator Stopper"
            echo ""
            echo "Usages:"
            echo "  abd-emulator-stop.sh -p port"
            echo ""
            echo "Options:"
            echo "  -p PORT    Port of the emulator to stop."
            echo ""
            exit
            ;;
    esac
done
shift $((OPTIND -1))

# ======================================================================================================================

if [[ "${PORT}" = "" ]]
then
    echo "-p PORT is required."
    exit 1
fi

if [[ ! ${PORT} =~ ^-?[0-9]+$ ]]
then
    echo "Invalid Port Number."
    exit 1
fi

# ======================================================================================================================

EXTENSION=""
if [[ "$(uname)" == *MINGW* ]]
then
    echo "MINGW detected."
    EXTENSION=".exe"
else
    if [ "$(ls -A ${ANDROID_HOME}/cmdline-tools/latest/bin)" ]
    then
        echo "New Tools Path detected."
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 17"
            brew upgrade jenv
            jenv local 17.0
        fi
    else
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 1.8"
            brew upgrade jenv
            jenv local 1.8
        fi
    fi
fi

# ======================================================================================================================

echo "Stopping Emulator on Port: 55${PORT}"

${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT} emu kill
