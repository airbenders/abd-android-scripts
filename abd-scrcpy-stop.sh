#!/usr/bin/env bash

while getopts 'p:f:' opt
do
    case ${opt} in
        p) SCRCPY_PID=${OPTARG} ;;
        f) SCRCPY_FILE=${OPTARG} ;;
        \?)
            echo "ABD Screen Copy Stopper"
            echo ""
            echo "Usages:"
            echo "  abd-scrcpy-stop.sh -p port -f scrcpy_file"
            echo ""
            echo "Options:"
            echo "  -p SCRCPY_PID       Screen recorder process ID."
            echo "  -f SCRCPY_FILE      Screen recorder video file."
            echo ""
            exit
            ;;
    esac
done

if [[ "${SCRCPY_PID}" = "" ]]
then
    echo "-p SCRCPY_PID is required."
    exit 1
fi

if [[ ! ${SCRCPY_PID} =~ ^-?[0-9]+$ ]]
then
    echo "Invalid SCRCPY_PID"
    exit 1
fi

if [[ "${SCRCPY_FILE}" = "" ]]
then
    echo "-n SCRCPY_FILE is required."
    exit 1
fi

echo "Stopping Screen Recorder: ${SCRCPY_PID}"
kill -INT ${SCRCPY_PID} & sleep 5

echo "Deleting ${SCRCPY_FILE}"
rm "${SCRCPY_FILE}"
