#!/usr/bin/env bash

APK_FILE="$1"

# VALIDATIONS ======================================================================================

echo "VALIDATION"
echo

if [[ ${ANDROID_HOME} = "" ]]
then
	echo
	echo "    * ANDROID_HOME environment not found!"
	echo
	exit 1
else
	echo "    * ANDROID_HOME environment found"
fi

if [[ ! -x "$(command -v ${ANDROID_HOME}/platform-tools/adb)" ]]
then
	echo
	echo "    * ADB not found"
	echo
	exit 1
else
	echo "    * ADB found"
fi

if [[ ! -x "$(command -v ${ANDROID_HOME}/tools/bin/apkanalyzer)" ]]
then
	echo
	echo "    * APKAnalyzer not found!"
	echo
	exit 1
else
	echo "    * APKAnalyzer found"
fi

APK_FOUND=false

if [[ "${APK_FILE}" = "" ]]
then
    # If there's no APK file provided
    SCAN=true
    echo "    * Searching for APKs..."

    for file in *.apk
	do
	    APK_FILE="$file"
	done

	if [[ ! ${APK_FILE} = "*.apk" ]]
    then
        APK_FOUND=true
    fi
else
    # if there's APK file provided
    SCAN=false
    if [[ -f "${APK_FILE}" ]]
    then
        APK_FOUND=true
    fi
fi

if [[ ${APK_FOUND} = "false" ]]
then
	echo "    * APK not found! ${APK_FILE}"
	pwd
	exit 1
fi

echo "    * APK file found"

if [[ ${SCAN} = "true" ]]
then
    for file in *.obb
	do
	    OBB_FILE="$file"
	done

	if [[ ! ${OBB_FILE} = "*.obb" ]]
    then
        echo "    * OBB file found"
    else
        OBB_FILE=""
    fi
fi

# ==================================================================================================

EXTENSION=""
if [[ "$(uname)" == *MINGW* ]]
then
    echo "MINGW detected."
    EXTENSION=".bat"
else
    if [ "$(ls -A ${ANDROID_HOME}/cmdline-tools/latest/bin)" ]
    then
        echo "New Tools Path detected."
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 17"
            brew upgrade jenv
            jenv local 17.0
        fi
    else
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 1.8"
            brew upgrade jenv
            jenv local 1.8
        fi
    fi
fi

APK_ANALYZER=""
if [[ -f "${ANDROID_HOME}/cmdline-tools/latest/bin/apkanalyzer${EXTENSION}" ]]
then
    APK_ANALYZER=${ANDROID_HOME}/cmdline-tools/latest/bin/apkanalyzer${EXTENSION}
elif [[ -f "${ANDROID_HOME}/tools/bin/apkanalyzer${EXTENSION}" ]]
then
    APK_ANALYZER=${ANDROID_HOME}/tools/bin/apkanalyzer${EXTENSION}
else
    echo "Unable to find apkanalyzer."
    exit 1
fi

# ==================================================================================================

echo
echo "ANALYZATION"
echo

if [[ ! ${OBB_FILE} = "" ]]
then
    echo "    * OBB to analyze: ${OBB_FILE}"
fi

echo "    * APK to analyze: ${APK_FILE}"

APPLICATION_ID=$(${APK_ANALYZER} manifest application-id "${APK_FILE}")
echo "    * Application ID: ${APPLICATION_ID}"

# [PAUL] This doesn't work
#VERSION_NAME=$(${APK_ANALYZER} manifest version-name "${APK_FILE}")
VERSION_NAME=$(${APK_ANALYZER} manifest print "${APK_FILE}" | grep "android:versionName" | awk 'BEGIN { FS="=" } { gsub("\"","") ; print $2 }')
echo "    * Version Name: ${VERSION_NAME}"

VERSION_CODE=$(${APK_ANALYZER} manifest version-code "${APK_FILE}")
echo "    * Version Code: ${VERSION_CODE}"

MIN_SDK_API=$(${APK_ANALYZER} manifest min-sdk "${APK_FILE}")
echo "    * Min SDK (API): ${MIN_SDK_API}"

if [[ "${MIN_SDK_API}" = "3" ]]
then
	MIN_SDK_NAME="Android 1.5 Cupcake"
elif [[ "${MIN_SDK_API}" = "4" ]]
then
	MIN_SDK_NAME="Android 1.6 Donut"
elif [[ "${MIN_SDK_API}" = "5" ]]
then
	MIN_SDK_NAME="Android 2.0 Eclair"
elif [[ "${MIN_SDK_API}" = "6" ]]
then
	MIN_SDK_NAME="Android 2.0.1 Eclair"
elif [[ "${MIN_SDK_API}" = "7" ]]
then
	MIN_SDK_NAME="Android 2.1 Eclair"
elif [[ "${MIN_SDK_API}" = "8" ]]
then
	MIN_SDK_NAME="Android 2.2.x Froyo"
elif [[ "${MIN_SDK_API}" = "9" ]]
then
	MIN_SDK_NAME="Android 2.3 - 2.3.2 Gingerbread"
elif [[ "${MIN_SDK_API}" = "10" ]]
then
	MIN_SDK_NAME="Android 2.3.3 - 2.3.7 Gingerbread"
elif [[ "${MIN_SDK_API}" = "11" ]]
then
	MIN_SDK_NAME="Android 3.0 Honeycomb"
elif [[ "${MIN_SDK_API}" = "12" ]]
then
	MIN_SDK_NAME="Android 3.1 Honeycomb"
elif [[ "${MIN_SDK_API}" = "13" ]]
then
	MIN_SDK_NAME="Android 3.2.x Honeycomb"
elif [[ "${MIN_SDK_API}" = "14" ]]
then
	MIN_SDK_NAME="Android 4.0.1 - 4.0.2 Ice Cream Sandwich"
elif [[ "${MIN_SDK_API}" = "15" ]]
then
	MIN_SDK_NAME="Android 4.0.3 - 4.0.4 Ice Cream Sandwich"
elif [[ "${MIN_SDK_API}" = "16" ]]
then
	MIN_SDK_NAME="Android 4.1.x Jelly Bean"
elif [[ "${MIN_SDK_API}" = "17" ]]
then
	MIN_SDK_NAME="Android 4.2.x Jelly Bean"
elif [[ "${MIN_SDK_API}" = "18" ]]
then
	MIN_SDK_NAME="Android 4.3.x Jelly Bean"
elif [[ "${MIN_SDK_API}" = "19" ]]
then
	MIN_SDK_NAME="Android 4.4 - 4.4.4 KitKat"
elif [[ "${MIN_SDK_API}" = "20" ]]
then
	MIN_SDK_NAME="Android 4.4W KitKat"
elif [[ "${MIN_SDK_API}" = "21" ]]
then
	MIN_SDK_NAME="Android 5.0 Lollipop"
elif [[ "${MIN_SDK_API}" = "22" ]]
then
	MIN_SDK_NAME="Android 5.1 Lollipop"
elif [[ "${MIN_SDK_API}" = "23" ]]
then
	MIN_SDK_NAME="Android 6.0 Marshmallow"
elif [[ "${MIN_SDK_API}" = "24" ]]
then
	MIN_SDK_NAME="Android 7.0 Nougat"
elif [[ "${MIN_SDK_API}" = "25" ]]
then
	MIN_SDK_NAME="Android 7.1 Nougat"
elif [[ "${MIN_SDK_API}" = "26" ]]
then
	MIN_SDK_NAME="Android 8.0.0 Oreo"
elif [[ "${MIN_SDK_API}" = "27" ]]
then
	MIN_SDK_NAME="Android 8.1.0 Oreo"
elif [[ "${MIN_SDK_API}" = "28" ]]
then
	MIN_SDK_NAME="Android 9 Pie"
elif [[ "${MIN_SDK_API}" = "29" ]]
then
	MIN_SDK_NAME="Android 10 Quince Tart"
elif [[ "${MIN_SDK_API}" = "30" ]]
then
	MIN_SDK_NAME="Android 11 Red Velvet Cake"
elif [[ "${MIN_SDK_API}" = "31" ]]
then
	MIN_SDK_NAME="Android 12 Snow Cone"
elif [[ "${MIN_SDK_API}" = "32" ]]
then
	MIN_SDK_NAME="Android 12L Snow Cone"
elif [[ "${MIN_SDK_API}" = "33" ]]
then
	MIN_SDK_NAME="Android 13 Tiramisu"
elif [[ "${MIN_SDK_API}" = "34" ]]
then
	MIN_SDK_NAME="Android 14 Upside Down Cake"
elif [[ "${MIN_SDK_API}" = "35" ]]
then
	MIN_SDK_NAME="Android 15 Vanilla Ice Cream"
else
	MIN_SDK_NAME="Unrecognized Android Version"
fi

echo "    * Min SDK (NAME): ${MIN_SDK_NAME}"

${APK_ANALYZER} files list "${APK_FILE}" >> ${TEMP_PATH}output.txt

# ARM 32 CHECK

cat ${TEMP_PATH}output.txt | grep "/lib/armeabi-v7a/" -q
EXIT_CODE=$?
HAS_ARM32=yes
if [[ ${EXIT_CODE} -gt 0 ]]
then
    HAS_ARM32=no
fi

# ARM 64 CHECK

cat ${TEMP_PATH}output.txt | grep "/lib/arm64-v8a/" -q
EXIT_CODE=$?
HAS_ARM64=yes
if [[ ${EXIT_CODE} -gt 0 ]]
then
    HAS_ARM64=no
fi

# INTEL 32 CHECK

cat ${TEMP_PATH}output.txt | grep "/lib/x86/" -q
EXIT_CODE=$?
HAS_INTEL32=yes
if [[ ${EXIT_CODE} -gt 0 ]]
then
    HAS_INTEL32=no
fi

# INTEL 64 CHECK

cat ${TEMP_PATH}output.txt | grep "/lib/x86_64/" -q
EXIT_CODE=$?
HAS_INTEL64=yes
if [[ ${EXIT_CODE} -gt 0 ]]
then
    HAS_INTEL64=no
fi

rm ${TEMP_PATH}output.txt

if [[ "${HAS_ARM32}" = "no" ]] && [[ "${HAS_ARM64}" = "no" ]] && [[ "${HAS_INTEL32}" = "no" ]] && [[ "${HAS_INTEL64}" = "no" ]]
then
	echo "    * Has Native Libraries: no"
else
	echo "    * Has Native ARM32 Libraries: ${HAS_ARM32}"
	echo "    * Has Native ARM64 Libraries: ${HAS_ARM64}"
	echo "    * Has Native INTEL32 Libraries: ${HAS_INTEL32}"
	echo "    * Has Native INTEL64 Libraries: ${HAS_INTEL64}"
fi
