#!/usr/bin/env bash

EMULATOR_NAME=""

while getopts 'n:?' opt
do
    case ${opt} in
        n) EMULATOR_NAME=${OPTARG} ;;
        \?)
            echo "ABD Emulator Deleter"
            echo ""
            echo "Usage:"
            echo "  abd-emulator-delete.sh -n emulator_name"
            echo ""
            echo "Options:"
            echo "  -n EMULATOR_NAME    Name of the emulator."
            echo ""
            exit
            ;;
    esac
done
shift $((OPTIND -1))

# ======================================================================================================================

if [[ "${EMULATOR_NAME}" = "" ]]
then
    echo "-n EMULATOR_NAME is required."
    exit 1
fi

# ======================================================================================================================

EXTENSION=""
if [[ "$(uname)" == *MINGW* ]]
then
    echo "MINGW detected."
    EXTENSION=".bat"
else
    if [ "$(ls -A ${ANDROID_HOME}/cmdline-tools/latest/bin)" ]
    then
        echo "New Tools Path detected."
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 17"
            brew upgrade jenv
            jenv local 17.0
        fi
    else
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 1.8"
            brew upgrade jenv
            jenv local 1.8
        fi
    fi
fi

# ======================================================================================================================

AVD_MANAGER=""
if [[ -f "${ANDROID_HOME}/cmdline-tools/latest/bin/avdmanager${EXTENSION}" ]]
then
    AVD_MANAGER=${ANDROID_HOME}/cmdline-tools/latest/bin/avdmanager${EXTENSION}
elif [[ -f "${ANDROID_HOME}/tools/bin/avdmanager${EXTENSION}" ]]
then
    AVD_MANAGER=${ANDROID_HOME}/tools/bin/avdmanager${EXTENSION}
else
    echo "Unable to find avdmanager."
    exit 1
fi

echo "Using AVD Manager: ${AVD_MANAGER}"

# ======================================================================================================================

echo "Deleting Emulator: ${EMULATOR_NAME}"

${AVD_MANAGER} delete avd --name ${EMULATOR_NAME}
