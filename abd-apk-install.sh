#!/usr/bin/env bash

NO_OVERWRITE=false
AUTO_RUN=true

while getopts 'a:o:e:nr?' opt
do
    case ${opt} in
        a) APK_FILE="${OPTARG}" ;;
        o) OBB_FILE="${OPTARG}" ;;
        e) EMULATOR=${OPTARG} ;;
        n) NO_OVERWRITE=true ;;
        r) AUTO_RUN=false ;;
        \?)
            echo "ABD APK installer"
            echo
            echo "Usages:"
            echo "  abd-apk-install.sh [-a APK_FILE] [-o OBB_FILE] [-e EMULATOR] [-n] [-r]"
            echo
            echo "Options:"
            echo "  -a APK_FILE             APK file to install."
            echo "  -o OBB_FILE             OBB file to install."
            echo "  -e EMULATOR             The target android device."
            echo "  -n                      Will not overwrite existing app if found."
            echo "  -r                      Will disable application auto run if provided."
            echo
            exit
            ;;
    esac
done

# ==================================================================================================

source $(dirname $0)/abd-base-apk.sh "${APK_FILE}"

# ==================================================================================================

function install_apk(){
    echo "    * Installing APK..."
	${ANDROID_HOME}/platform-tools/adb \
	    ${EMULATOR_PARAMETER} \
	    install \
	    "${APK_FILE}"

	EXIT_CODE=$?
	if [[ ${EXIT_CODE} -gt 0 ]]
	then
		echo "    * Failed to install APK"
	    exit 1
	fi
}

function upload_obb() {
    OBB_DESTINATION="/storage/emulated/0/Android/obb/${APPLICATION_ID}/main.${VERSION_CODE}.${APPLICATION_ID}.obb"
    echo "    * Uploading OBB to: ${OBB_DESTINATION}"

    ${ANDROID_HOME}/platform-tools/adb \
        ${EMULATOR_PARAMETER} \
        push \
        "${OBB_FILE}" \
        "${OBB_DESTINATION}"

    EXIT_CODE=$?
    if [[ ${EXIT_CODE} -gt 0 ]]
    then
        echo "* Failed to upload APK"
        exit 1
    fi
}

echo
echo "OPERATION"
echo

if [[ ${EMULATOR} = "" ]]
then
	EMULATOR=$(adb devices | awk 'FNR==2 {print $1}')
fi

if [[ ${EMULATOR} = "" ]]
then
	echo "    * Emulator Not Found!"
	exit 1
else
	echo "    * Emulator Found: ${EMULATOR}"
	EMULATOR_PARAMETER="-s ${EMULATOR}"
fi

${ANDROID_HOME}/platform-tools/adb \
    ${EMULATOR_PARAMETER} \
    shell \
    pm list packages \
    | \
    grep ${APPLICATION_ID} -q
FOUND=$?

if [[ ${FOUND} -eq 0 ]]
then
	if [[ ${NO_OVERWRITE} = "true" ]]
	then
		echo "    * Installed APK found, skipping installation..."
	else
		echo "    * Uninstalling installed APK..."
		${ANDROID_HOME}/platform-tools/adb \
		    ${EMULATOR_PARAMETER} \
		    shell am force-stop ${APPLICATION_ID}
		${ANDROID_HOME}/platform-tools/adb \
		    ${EMULATOR_PARAMETER} \
		    shell pm uninstall ${APPLICATION_ID}

		install_apk
	fi
else
    install_apk
fi

if [[ "${OBB_FILE}" != "" ]]
then
    upload_obb
    OBB_INSTALLED="+OBB"
fi

if [[ "${AUTO_RUN}" = "true" ]]
then
    echo "    * Launching APK..."
    echo

    ${ANDROID_HOME}/platform-tools/adb \
        shell monkey \
        -p ${APPLICATION_ID} \
        -c android.intent.category.LAUNCHER 1 > /dev/null 2>&1
fi

echo "APK${OBB_INSTALLED} INSTALLED SUCCESSFULLY!"
