#!/usr/bin/env bash

DISPLAY=false

while getopts 's:f:pd?' opt
do
    case ${opt} in
        s) SERIAL=${OPTARG} ;;
        f) SCRCPY_FILE=${OPTARG} ;;
        p) SCRCPY_PORT=${OPTARG} ;;
        d) DISPLAY=true ;;
        \?)
            echo "ABD Screen Copy"
            echo ""
            echo "Usages:"
            echo "  abd-scrcpy-start.sh -s SERIAL -f scrcpy_file -p port [-d]"
            echo ""
            echo "Options:"
            echo "  -s SERIAL           Device Serial."
            echo "  -f SCRCPY_FILE      Screen recorder video file."
            echo "  -p SCRCPY_PORT      Screen recorder port number to use."
            echo "  -d                  Display screen copy."
            echo ""
            exit
            ;;
    esac
done

if [[ "${SERIAL}" = "" ]]
then
    echo "-s SERIAL is required."
    exit 1
fi

if [[ "${SCRCPY_FILE}" = "" ]]
then
    echo "-f SCRCPY_FILE is required."
    exit 1
fi

if [[ "${DISPLAY}" = "false" ]]
    then
        DISPLAY="--no-window"
    else
        DISPLAY=""
fi

echo "Installing / Updating SCRCPY"

if [[ -x "$(command -v jenv)" ]]
then
    echo "Setting Java Version to 17"
    brew upgrade jenv
    jenv local 17.0
fi

if [[ ! -x "$(command -v scrcpy)" ]]
then
    brew install scrcpy
else
    brew upgrade scrcpy
fi

if [[ "${SCRCPY_PORT}" = "" ]]
then
    scrcpy \
    --serial ${SERIAL} \
    --no-playback \
    ${DISPLAY} \
    --no-control \
    --record "${SCRCPY_FILE}" \
    & \
    SCRCPY_PID=$!
else
    scrcpy \
    --port "271${SCRCPY_PORT}" \
    --serial ${SERIAL} \
    --no-playback \
    ${DISPLAY} \
    --no-control \
    --record "${SCRCPY_FILE}" \
    & \
    SCRCPY_PID=$!
fi

echo "Screen Recorder Started"

if [[ "${SCRCPY_PORT}" != "" ]]
then
  echo "Port: 271${SCRCPY_PORT}"
fi

echo "PID: ${SCRCPY_PID}"
echo "File: ${SCRCPY_FILE}"
echo "Display: ${DISPLAY}"
