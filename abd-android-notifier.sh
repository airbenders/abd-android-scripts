#!/usr/bin/env bash

SERIAL=""
LEVEL=""
MESSAGE=""

while getopts 's:l:m:?' opt
do
    case ${opt} in
        s) SERIAL=${OPTARG} ;;
        l) LEVEL=${OPTARG} ;;
        m) MESSAGE=${OPTARG} ;;
        \?)
            echo "ABD Notifier"
            echo ""
            echo "Usages:"
            echo "  abd-android-notifier.sh -s SERIAL -l LEVEL -m MESSAGE"
            echo ""
            echo "Options:"
            echo "  -s SERIAL          Device Serial."
            echo "  -l LEVEL           Level of the message."
            echo "  -m MESSAGE         Message."
            echo ""
            exit
            ;;
    esac
done

if [[ "${SERIAL}" = "" ]]
then
    echo "-s Device SERIAL is required."
    exit 1
fi

if [[ "${LEVEL}" = "" ]]
then
    echo "-l LEVEL is required."
    exit 1
fi

if [[ "${MESSAGE}" = "" ]]
then
    echo "-m MESSAGE is required."
    exit 1
fi

if [[ -x "$(command -v jenv)" ]]
then
    echo "Setting Java Version to 1.8"
    brew upgrade jenv
    jenv local 1.8
fi

${ANDROID_HOME}/platform-tools/adb \
    -s ${SERIAL} \
    shell am force-stop com.abd.androidnotifier

${ANDROID_HOME}/platform-tools/adb \
    -s ${SERIAL} \
    shell "am start -n com.abd.androidnotifier/.MainActivity --es '${LEVEL}' '${MESSAGE}'"
