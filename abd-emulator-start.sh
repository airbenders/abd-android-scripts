#!/usr/bin/env bash

EMULATOR_NAME=""
PORT_1=""
PORT_2=""
WITH_HEAD=true
WITH_ANIMATIONS=false
WITH_HEADS_UP_NOTIFICATION=false
SAMPLE_IMAGE=""
ARCORE=""
NOTIFIER=""
LONGITUDE=-80.1289
LATITUDE=26.3683

while getopts 'n:p:seux:y:i:a:f:?' opt
do
    case ${opt} in
        n) EMULATOR_NAME=${OPTARG} ;;
        p)
            PORT_1=$(expr ${OPTARG} + 0)
            PORT_2=$(expr ${PORT_1} + 1)
            ;;
        s) WITH_HEAD=false ;;
        e) WITH_ANIMATIONS=true ;;
        u) WITH_HEADS_UP_NOTIFICATION=true ;;
        x) LONGITUDE=${OPTARG} ;;
        y) LATITUDE=${OPTARG} ;;
        i) SAMPLE_IMAGE=${OPTARG} ;;
        a) ARCORE=${OPTARG} ;;
        f) NOTIFIER=${OPTARG} ;;
        \?)
            echo "ABD Emulator Starter"
            echo ""
            echo "Usages:"
            echo "  abd-emulator-start.sh -n NAME -p PORT [-s] [-e] [-u] [-x LONGITUDE] [-y LATITUDE] [-i IMAGE] [-a ARCODE] [-f NOTIFIER]"
            echo ""
            echo "Options:"
            echo "  -n EMULATOR_NAME    Name of the emulator."
            echo "  -p PORT             Port of the emulator."
            echo "  -s                  Create headless emulator."
            echo "  -e                  Enable animations."
            echo "  -u                  Enable heads up notification."
            echo "  -x LONGITUDE        Sets the longitude."
            echo "  -y LATITUDE         Sets the latitude."
            echo "  -i IMAGE            Add sample image to emulator."
            echo "  -a ARCORE_APK       Install ARCore to emulator."
            echo "  -f NOTIFIER         Install Notifier to emulator."
            echo ""
            exit
            ;;
    esac
done
shift $((OPTIND -1))

# ======================================================================================================================

if [[ "${EMULATOR_NAME}" = "" ]]
then
    echo "-n EMULATOR_NAME is required."
    exit 1
fi

if [[ "${PORT_1}" = "" ]]
then
    echo "-p PORT is required."
    exit 1
fi

if [[ ! ${PORT_1} =~ ^-?[0-9]+$ ]]
then
    echo "Invalid Port Number."
    exit 1
fi

# ======================================================================================================================

EXTENSION=""
if [[ "$(uname)" == *MINGW* ]]
then
    echo "MINGW detected."
    EXTENSION=".exe"
else
    if [ "$(ls -A ${ANDROID_HOME}/cmdline-tools/latest/bin)" ]
    then
        echo "New Tools Path detected."
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 17"
            brew upgrade jenv
            jenv local 17.0
        fi
    else
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 1.8"
            brew upgrade jenv
            jenv local 1.8
        fi
    fi
fi

# ======================================================================================================================

echo
echo "Starting Emulator: 55${PORT_1}, 55${PORT_2}"
echo "With Head: ${WITH_HEAD}"
echo

if [[ "${WITH_HEAD}" = "true" ]]
then
    EMULATOR=emulator${EXTENSION}
    ARGUMENTS=""
else
    if [[ -f "${ANDROID_HOME}/emulator/emulator-headless${EXTENSION}" ]]
    then
        EMULATOR=emulator-headless${EXTENSION}
    else
        EMULATOR=emulator${EXTENSION}
    fi

    ARGUMENTS="-no-window"
fi

${ANDROID_HOME}/emulator/${EMULATOR} \
    -ports 55${PORT_1},55${PORT_2} @${EMULATOR_NAME} \
    -no-snapshot \
    -no-audio \
    -no-boot-anim \
    -gpu auto \
    -camera-back virtualscene \
    -camera-front emulated \
    -memory 2047 \
    -partition-size 2047 \
    -timezone America/Los_Angeles \
    ${ARGUMENTS} \
    & \
    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} \
    -s emulator-55${PORT_1} \
    wait-for-device shell 'while [[ -z $(getprop sys.boot_completed) ]]; do sleep 1; done; input keyevent 82'

echo "Showing Touches"

# This first command loop is needed to unsure that the emulator is ready.
EXIT_CODE=1
until [[ ${EXIT_CODE} -eq 0 ]]
do
    sleep 3
    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put system show_touches 1 > /dev/null 2>&1
    EXIT_CODE=$?
done

echo "Setting GPS Location to ${LONGITUDE} ${LATITUDE}"

${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} emu geo fix ${LONGITUDE} ${LATITUDE}

echo "Disabling Autocomplete"

${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put secure autofill_service null

if [[ ! "${WITH_ANIMATIONS}" = "true" ]]
then
    echo "Disabling Animations"

    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global window_animation_scale 0.0
    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global transition_animation_scale 0.0
    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global animator_duration_scale 0.0
fi

if [[ ! "${WITH_HEADS_UP_NOTIFICATION}" = "true" ]]
then
    echo "Disabling Heads-Up Notification"

    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global heads_up_notifications_enabled 0
fi

echo "Disabling Auto Timezone"

${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global auto_time_zone 0
${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put global auto_time 0
${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell settings put system time_12_24 24

if [[ ! "${SAMPLE_IMAGE}" = "" ]]
then
    echo "Adding Sample Image"

    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} push ${SAMPLE_IMAGE} /sdcard/Download/${SAMPLE_IMAGE}
    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d file:///sdcard/Download/${SAMPLE_IMAGE}
fi

if [[ ! "${ARCORE}" = "" ]]
then
    echo "Installing ARCore"

    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} install ${ARCORE}
fi

if [[ ! "${NOTIFIER}" = "" ]]
then
    echo "Installing Notifier"

    ${ANDROID_HOME}/platform-tools/adb${EXTENSION} -s emulator-55${PORT_1} install ${NOTIFIER}
fi

echo "Emulator Started"
