#!/usr/bin/env bash

PORT=$1

# ABD Emulator Starter
#
# Usages:
#  abd-apk-uninstall.sh <PORT> <PACKAGES...>
#
# Options:
#   PORT     Port of the emulator.
#   PACKAGES Packages to be uninstalled.

uninstall () {
    ${ANDROID_HOME}/platform-tools/adb -s $1 shell pm list packages | grep $2
    if [[ $? -eq 0 ]]
    then
        echo "Uninstalling $2 on $1"
        ${ANDROID_HOME}/platform-tools/adb -s $1 shell pm uninstall $2
    fi
}

if [[ -x "$(command -v jenv)" ]]
then
    echo "Setting Java Version to 1.8"
    brew upgrade jenv
    jenv local 1.8
fi

uninstall emulator-55${PORT} io.appium.settings
uninstall emulator-55${PORT} io.appium.unlock
uninstall emulator-55${PORT} io.appium.uiautomator2.server.test
uninstall emulator-55${PORT} io.appium.uiautomator2.server
uninstall emulator-55${PORT} com.linkedin.testbutler

shift

for PACKAGE in "$@"
do
    uninstall emulator-55${PORT} ${PACKAGE}
    uninstall emulator-55${PORT} ${PACKAGE}.test
done
