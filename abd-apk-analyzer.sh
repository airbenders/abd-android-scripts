#!/usr/bin/env bash

while getopts 'a:' opt
do
    case ${opt} in
        a) APK_FILE=${OPTARG} ;;
        \?)
            echo "ABD APK analyzer"
            echo ""
            echo "Usages:"
            echo "  abd-apk-analyzer.sh -a APK_FILE"
            echo ""
            echo "Options:"
            echo "  -a APK_FILE             APK file to analyze."
            echo ""
            exit
            ;;
    esac
done

if [[ "${APK_FILE}" = "" ]]
then
    echo "-a APK File is required."
    exit 1
fi

# ==================================================================================================

bash $(dirname $0)/abd-base-apk.sh ${APK_FILE}
