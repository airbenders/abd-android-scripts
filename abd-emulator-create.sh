#!/usr/bin/env bash

EMULATOR_NAME=""
API=""
DEVICE=""
ABI="google_apis"

while getopts 'n:a:d:po?' opt
do
    case ${opt} in
        n) EMULATOR_NAME=${OPTARG} ;;
        a) API=${OPTARG} ;;
        d) DEVICE=${OPTARG} ;;
        p) ABI="google_apis_playstore" ;;
        o) ABI="default" ;;
        \?)
            echo "ABD Emulator Creator"
            echo ""
            echo "Usages:"
            echo "  abd-emulator-create.sh -n name -d device -a api_version [-p] [-o]"
            echo ""
            echo "Options:"
            echo "  -n EMULATOR_NAME    Name of the emulator."
            echo "  -d DEVICE           Target android device."
            echo "  -a API_VERSION      Target API Version."
            echo "  -p                  Google Play Store ABI. (Optional)"
            echo "  -o                  Default ABI, no Google ABIs. (Optional)"
            echo ""
            exit
            ;;
    esac
done
shift $((OPTIND -1))

# ======================================================================================================================

if [[ "${EMULATOR_NAME}" = "" ]]
then
    echo "-n EMULATOR_NAME is required."
    exit 1
fi

if [[ "${DEVICE}" = "" ]]
then
    echo "-d DEVICE is required."
    exit 1
fi

if [[ "${API}" = "" ]]
then
    echo "-a API is required."
    exit 1
fi

# ======================================================================================================================

EXTENSION=""
if [[ "$(uname)" == *MINGW* ]]
then
    echo "MINGW detected."
    EXTENSION=".bat"
else
    if [ "$(ls -A ${ANDROID_HOME}/cmdline-tools/latest/bin)" ]
    then
        echo "New Tools Path detected."
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 17"
            brew upgrade jenv
            jenv local 17.0
        fi
    else
        if [[ -x "$(command -v jenv)" ]]
        then
            echo "Setting Java Version to 1.8"
            brew upgrade jenv
            jenv local 1.8
        fi
    fi
fi

# ======================================================================================================================

SDK_MANAGER=""
if [[ -f "${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager${EXTENSION}" ]]
then
    SDK_MANAGER=${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager${EXTENSION}
elif [[ -f "${ANDROID_HOME}/tools/bin/sdkmanager${EXTENSION}" ]]
then
    SDK_MANAGER=${ANDROID_HOME}/tools/bin/sdkmanager${EXTENSION}
else
    echo "Unable to find sdkmanager."
    exit 1
fi

echo "Using SDK Manager: ${SDK_MANAGER}"

# ======================================================================================================================

AVD_MANAGER=""
if [[ -f "${ANDROID_HOME}/cmdline-tools/latest/bin/avdmanager${EXTENSION}" ]]
then
    AVD_MANAGER=${ANDROID_HOME}/cmdline-tools/latest/bin/avdmanager${EXTENSION}
elif [[ -f "${ANDROID_HOME}/tools/bin/avdmanager${EXTENSION}" ]]
then
    AVD_MANAGER=${ANDROID_HOME}/tools/bin/avdmanager${EXTENSION}
else
    echo "Unable to find avdmanager."
    exit 1
fi

echo "Using AVD Manager: ${AVD_MANAGER}"

# ======================================================================================================================

ARCH="x86_64"
if [ "$(uname -m)" = "arm64" ]; then
    echo "Using ARM64 architecture."
    ARCH="arm64-v8a"
fi

# ======================================================================================================================

# Disabled for now, the latest emulator has some bugs
# https://www.reddit.com/r/androiddev/comments/1bqsb5z/running_an_emulator_on_a_second_display/
#echo "Installing / Updating SDK"
#
#echo y | ${SDK_MANAGER} \
#    "platform-tools" \
#    "emulator" \
#    "system-images;android-${API};${ABI};${ARCH}" \
#    --verbose

echo "Creating Emulator: ${EMULATOR_NAME}"

echo | ${AVD_MANAGER} \
    create avd \
    --name "${EMULATOR_NAME}" \
    --package "system-images;android-${API};${ABI};${ARCH}" \
    --sdcard 1024M \
    --force \
    --device "${DEVICE}"
